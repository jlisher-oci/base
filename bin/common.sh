#!/usr/bin/env bash

## Define our base functionality. Any and all common functionality used by the scripts should be defined here
function jlisher_common() {
  # cd into the base directory and make sure we can get back again using `leave_base_directory`
  function enter_base_directory() {
    [[ "${_ORIG_PWD:-}" ]] || {
      local -xrg _ORIG_PWD="${PWD}"
    }

    cd "${0%/*}/../"
  }

  # cd back to the original directory
  function leave_base_directory() {
    cd "${_ORIG_PWD}"
  }

  # check if we are in the original directory
  function is_original_directory() {
    # if `$_ORIG_PWD` is not set then we probably haven't changes directory
    [[ "${_ORIG_PWD:-}" ]] || {
      return 0
    }

    # compare `$_ORIG_PWD` and $PWD`
    [[ "${_ORIG_PWD}" != "${PWD}" ]] || {
      return 0
    }

    # If we made it here, return with non-zero exit code
    return 1
  }

  # define our trap function which will handle all ERR signals
  function trap_err() {
    # remove the debugging option, it's a bit much
    ! [[ "${DEBUG:-}" ]] || {
      set +x
    }

    # define an array to hold our output to be printed
    local -a output=()

    local file="${BASH_SOURCE[0]:-"${0}"}"
    local func_name="${FUNCNAME[1]:-"${file}"}"
    local -i line_no="${BASH_LINENO[0]:-0}"

    output+=("non-zero exit status in ${func_name}: ${file}:${line_no}")

    ! [[ "${VERBOSE:-}" ]] || {
      # add verbose output (the stacktrace)
      output+=("Stacktrace:")

      for i in "${!BASH_LINENO[@]}"; do
        file="${BASH_SOURCE[${i}]}"
        func_name="${FUNCNAME[${i}]:-"${file}"}"
        line_no="${BASH_LINENO[${i}]:-0}"

        output+=("  ${i}: ${file}:${line_no} in ${func_name}")
      done
    }

    # print the output
    for line in "${output[@]}"; do
      echo "ERROR: ${line}"
    done

    is_original_directory || {
      leave_base_directory
    }

    exit 1
  }

  # setup our shell's behaviour
  function run_main() {
    # define the input field separators to use
    IFS=$'\n\t'

    # see `help set` for more information
    set -ehmuBE -o pipefail

    # trap all ERR signals in our `trap_err` function
    trap trap_err ERR

    # add a debugging option, which outputs the commands as they are executed
    ! [[ "${DEBUG:-}" ]] || {
      set -x
    }

    # cd into the base directory
    enter_base_directory

    # source our .env file if it exists
    ! [[ -r "${PWD}/.env" ]] || {
      source "${PWD}/.env"
    }

    # execute the `main` function defined
    main

    # cd back to the origin directory
    leave_base_directory

    # @note we shouldn't need to worry about any of the commands exiting with a
    # non-zero exit code, as they should get handled by the `trap_err` function
    # and the shell options we setup for the this environment.
  }

  # define the main function to execute.
  # @todo: This function should be overwritten by the calling script.
  #   - Possibly should add a check for the existence and use a different name for the default.
  #   - Will add an issue where this can be further discussed.
  function main() {
    echo "--------------------------------------------------------------------------------"
    echo "Printing..."
    pwd
    echo "Printed"
    echo "--------------------------------------------------------------------------------"
  }
}
