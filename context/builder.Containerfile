ARG BASE_IMAGE
FROM ${BASE_IMAGE} AS builder

LABEL maintainer="jlisher Containers <containers@jlisher.com>" \
      vendor="jlisher" \
      name="builder" \
      description="A simple fedora based image, used to install packages in images."

# remove all unneeded files. some of these will be replaced next
RUN set -euBxo pipefail; \
  rm -rf /etc/{yum.repos.d,pki/rpm-gpg}/ /var/log/anaconda/ /root/;

# add the new config files and PGP keys
COPY ./rootfs/etc/ /etc/
COPY ./rootfs/root/ /root/

# update import PGP keys and the base image
RUN set -euBxo pipefail; \
  microdnf install dnf; \
  dnf swap libcurl libcurl-minimal; \
  dnf swap curl curl-minimal; \
  dnf swap coreutils coreutils-single; \
  dnf remove coreutils-common microdnf; \
  dnf upgrade; \
  dnf clean all; \
  rm -rf /{var/{cache,log,lib/{dnf,yum}}/*,etc/*/*.rpmnew}

# add the rootfs directory for building new images
COPY ./rootfs/ /rootfs/

# setup /rootfs to be used as the root directory for
RUN set -euBxo pipefail; \
  (rpm -E %fedora | tee /rootfs/etc/dnf/vars/releasever /etc/dnf/vars/releasever); \
  sed -i 's,installroot=/,installroot=/rootfs,' /etc/dnf/dnf.conf; \
  mkdir -p /rootfs/dev;
