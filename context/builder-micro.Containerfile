ARG IMAGE_NAME
FROM ${IMAGE_NAME}/builder AS builder-micro-builder

ARG IMAGE_NAME
COPY --from=${IMAGE_NAME}/dev / /rootfs/

RUN set -euBxo pipefail; \
  touch /rootfs/dev/null; \
  dnf install microdnf; \
  dnf clean all; \
  rm -rf {/rootfs,}/{var/{cache,log,lib/{dnf,yum}}/*,etc/*/*.rpmnew} /rootfs/dev/null;

FROM ${IMAGE_NAME}/dev AS builder-micro

LABEL name="builder-micro" \
      description="A simple fedora based minimal image, used to install packages in images."

COPY --from=builder-micro-builder /rootfs/ /

# add the rootfs directory for building new images
COPY ./rootfs/ /rootfs/

# setup /rootfs to be used as the root directory for
RUN set -euBxo pipefail; \
  mkdir -p /rootfs/dev; \
  touch /rootfs/dev/null; \
  (rpm -E %fedora | tee /rootfs/etc/dnf/vars/releasever /etc/dnf/vars/releasever); \
  sed -i 's,installroot=/,installroot=/rootfs,' /etc/dnf/dnf.conf; \
  rm -f /rootfs/dev/null;
