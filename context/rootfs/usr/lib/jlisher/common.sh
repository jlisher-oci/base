#!/usr/bin/env bash

################################################################################
# Define some base features and error handling                                 #
#                                                                              #
# The use of the ".sh" extension is to allow for future support for additional #
# languages.                                                                   #
################################################################################
function jlisher_common() {
  ################################################################################
  # Set the input field separator to newline and tabs only                       #
  ################################################################################
  IFS=$'\n\t'

  ################################################################################
  # Defines some print methods which handle colourisation                        #
  ################################################################################
  local -A STYLES=()

  STYLES[fg0]=""
  STYLES[fg1]=""
  STYLES[fg2]=""
  STYLES[fg3]=""
  STYLES[fg4]=""
  STYLES[fg5]=""
  STYLES[fg6]=""
  STYLES[fg7]=""

  STYLES[bg0]=""
  STYLES[bg1]=""
  STYLES[bg2]=""
  STYLES[bg3]=""
  STYLES[bg4]=""
  STYLES[bg5]=""
  STYLES[bg6]=""
  STYLES[bg7]=""

  STYLES[bold]=""
  STYLES[reset]=""

  [[ -z "${TERM:-}" ]] || {
    STYLES[fg0]="\[\e[0;30m\]"
    STYLES[fg1]="\[\e[0;31m\]"
    STYLES[fg2]="\[\e[0;32m\]"
    STYLES[fg3]="\[\e[0;33m\]"
    STYLES[fg4]="\[\e[0;34m\]"
    STYLES[fg5]="\[\e[0;35m\]"
    STYLES[fg6]="\[\e[0;36m\]"
    STYLES[fg7]="\[\e[0;37m\]"

    STYLES[bg0]="\[\e[0;40m\]"
    STYLES[bg1]="\[\e[0;41m\]"
    STYLES[bg2]="\[\e[0;42m\]"
    STYLES[bg3]="\[\e[0;43m\]"
    STYLES[bg4]="\[\e[0;44m\]"
    STYLES[bg5]="\[\e[0;45m\]"
    STYLES[bg6]="\[\e[0;46m\]"
    STYLES[bg7]="\[\e[0;47m\]"

    STYLES[bold]="\[\e[0;1m\]"
    STYLES[reset]="\[\e[0m\]"
  }

  STYLES[error]="${STYLES[bold]}${STYLES[fg1]}"
  STYLES[success]="${STYLES[bold]}${STYLES[fg2]}"
  STYLES[warning]="${STYLES[bold]}${STYLES[fg3]}"
  STYLES[info]="${STYLES[bold]}${STYLES[fg6]}"

  function print_plain() {
    for line in "${@}"; do
      echo -e "${line}"
    done
  }

  function print_info() {
    for line in "${@}"; do
      echo -e "${STYLES[info]}INFO: ${line}${STYLES[reset]}"
    done
  }

  function print_success() {
    for line in "${@}"; do
      echo -e "${STYLES[success]}SUCCESS: ${line}${STYLES[reset]}"
    done
  }

  function print_warning() {
    for line in "${@}"; do
      echo -e "${STYLES[warning]}WARNING: ${line}${STYLES[reset]}"
    done
  }

  function print_error() {
    for line in "${@}"; do
      echo -e "${STYLES[error]}ERROR: ${line}${STYLES[reset]}"
    done
  }

  ################################################################################
  # Setup the shell's behaviour                                                  #
  ################################################################################
  set -o pipefail
  set -o errtrace
  set -o errexit
  set -o braceexpand
  set -o monitor
  set -o hashall

  function trap_err() {
    ! [[ "${DEBUG}" ]] || set +o xtrace

    local func_name="${FUNCNAME[0]:-main}"
    local line_no="${BASH_LINENO[0]:-0}"

    local -a output=()

    # print the function and line where the error occurred
    output+=("non-zero exit status in ${func_name} at line: ${line_no}")
    output+=("Stacktrace:")

    for i in "${!BASH_LINENO[@]}"; do
      file="${BASH_SOURCE[${i}]}"
      func_name="${FUNCNAME[${i}]:-main}"
      line_no="${BASH_LINENO[${i}]:-0}"

      # prints file: function: line
      output+=("${i}: ${file}:${line_no} in ${func_name}")
    done

    print_error "${output[@]}"
    exit 1
  }

  trap trap_err ERR

  ################################################################################
  # Setup the debugging output.                                                  #
  # Add debugging functionality after the basic prints to try keep things clean. #
  ################################################################################
  ! [[ "${DEBUG:-}" ]] || set -o xtrace
}
