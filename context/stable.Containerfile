ARG IMAGE_NAME

FROM ${IMAGE_NAME}/builder AS stable-builder

RUN set -euBxo pipefail; \
  touch /rootfs/dev/null; \
  dnf install fedora-release-container coreutils-single libcurl-minimal; \
  dnf clean all; \
  rm -rf {/rootfs,}/{var/{cache,log,lib/{dnf,yum}}/*,etc/*/*.rpmnew} /rootfs/dev/null;

FROM scratch AS stable

LABEL maintainer="jlisher Containers <containers@jlisher.com>" \
      vendor="jlisher" \
      name="stable" \
      description="A simple minimal bash image, built with fedora."

COPY --from=stable-builder /rootfs/ /

# set the environment to run as root login
ENV HOME=/root \
    TERM=xterm
WORKDIR /root

ENTRYPOINT ["/usr/sbin/entrypoint"]
CMD ["/usr/bin/bash", "-l"]
